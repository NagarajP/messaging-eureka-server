package com.myzee.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class MessagingEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessagingEurekaServerApplication.class, args);
	}

}
